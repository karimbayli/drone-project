package models

import (
	"gorm.io/gorm"
	"time"
)

type Upload struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement;not null"`
	Name      string `json:"name"; gorm:"unique"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

type UploadFileResponse struct {
	FileName string `json:"filename"`
}
