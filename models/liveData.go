package models

import (
	"drone-project/db"
	"fmt"
	"strconv"
	"time"
)

type FlightStats struct {
	Latitude string
	Longitude string
	Radius string
}

type RecentDroneStats struct {
	Id uint `json:"flight_record_id"`
	DroneId uint `json:"drone_id"`
	Latitude    string `json:"latitude"`
	LatitudeFirstPart    string `json:"latitude_first_part"`
	LatitudeSecondPart    string `json:"latitude_second_part"`
	Longitude    string `json:"longitude"`
	LongitudeFirstPart    string `json:"longitude_first_part"`
	LongitudeSecondPart    string `json:"longitude_second_part"`
}


type DroneStatsForUser struct {
	Drone Drone `json:"drone"`
	Distance int `json:"distance"`
}



func (f FlightStats) FindNearestDrones() ([]DroneStatsForUser, error) {

	recentDronesStats, err := recentDrones()
	if err != nil {
		return nil,err
	}
	var droneStatsForUser []DroneStatsForUser


	userLatitude, _ := strconv.ParseFloat(f.Latitude, 64)
	userLongitude, _ := strconv.ParseFloat(f.Longitude, 64)
	userRadius, _ := strconv.Atoi(f.Radius)

	userCoordinate := Coordinate{
		Latitude:  userLatitude,
		Longitude: userLongitude,
	}

	for _, v := range recentDronesStats{
		destinationLatitude, _ :=  strconv.ParseFloat(v.Latitude, 64)
		destionationLongtitude, _ :=  strconv.ParseFloat(v.Longitude, 64)

		destinationCoordinate := Coordinate{
			Latitude:  destinationLatitude,
			Longitude: destionationLongtitude,
		}

		coordinatesXY := CoordinatesXY{
			Origin:      userCoordinate,
			Destination: destinationCoordinate,
		}

		distance := Distance(coordinatesXY)
		fmt.Println(int(distance*1000))
		 if userRadius >= int(distance*1000){

			drone,_ := findDroneById(v.DroneId)

			 droneStat :=DroneStatsForUser{
				 Drone:    drone,
				 Distance: int(distance*1000),
			 }

			 droneStatsForUser = append(droneStatsForUser, droneStat)
		}
	}




	return droneStatsForUser,nil

}

func recentDrones() ([]RecentDroneStats, error){
	now := time.Now()

	fiveMinBefore := now.Add(-5*time.Minute).UTC()
	db := db.GetDB()

	dronesStats := []RecentDroneStats{}

	errDb := db.Debug().Raw(" SELECT m.id,m.drone_id,m.latitude_first_part,m.latitude_second_part,m.latitude,m.longitude_first_part,m.longitude_second_part, m.longitude, t.mx FROM ( SELECT drone_id, MAX(id) AS mx FROM flight_records where created_at > ? GROUP BY drone_id ) t JOIN flight_records m ON m.drone_id = t.drone_id AND t.mx = m.id ", fiveMinBefore).Find(&dronesStats).Error
	if errDb != nil {
		return nil,errDb
	}

	return dronesStats, nil
}








