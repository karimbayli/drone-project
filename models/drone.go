package models

import (
	"drone-project/db"
	"drone-project/forms"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"strconv"
	"time"
)



type Drone struct {
	gorm.Model `json:"-"`
	ID          uint      `gorm:"primaryKey;autoIncrement;not null"`
	DroneId     uuid.UUID `json:"droneId", gorm:"primaryKey;autoIncrement;not null"`
	Name        string    `json:"name"`
	Issuer      string    `json:"issuer", gorm:"not null"`
	SerialNumber      string    `json:"serial_number" gorm:"unique;not null"`
	DroneTypeID int       `json:"type_id"`
	DroneType   DroneType `json:"-"gorm:"references:ID"`
	//User        User      `gorm:"foreignKey:ID"`
	OwnerId     string    `json:"owner_id"`
	OwnerName   string    `json:"owner_name"`
	Serial      string    `json:"serial"`
	CreatedAt   time.Time  `json:"-"; gorm:"autoCreateTime"`
	UpdatedAt time.Time    `json:"-"; gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `json:"-"; gorm:"index"`
}


type DroneType struct {
	gorm.Model `json:"-"`
	ID        uint `gorm:"primaryKey;autoIncrement"`
	TypeName      string `json:"type_name" gorm:"not null;unique"`
	Purpose  string `json:"purpose" gorm:"not null;default:personal user"`
	Type    string `json:"type"  gorm:"not null; default:civil"`
	CreatedAt time.Time `json:"-",gorm:"autoCreateTime"`
	UpdatedAt time.Time `json:"-",gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `json:"-",gorm:"index"`
}

type LiveDrone struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement"`
	Name      string `json:"name"`
	Registration  string `json:"registration"`
	Purpose  string `json:"purpose"`
	Type    string `json:"type"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type DroneReport struct {
	gorm.Model `json:"-"`
	ID        uint `gorm:"primaryKey;autoIncrement"`
	Message string `json:"message"`
	DroneId uint  `json:"drone_id"`
	Latitude    float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Photo       string `json:"img_path"`
	CreatedAt time.Time `json:"-" gorm:"autoCreateTime"`
	UpdatedAt time.Time `json:"-" gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

type FlightRecord struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement"`
	DroneID  string
	Drone Drone `gorm:"foreignKey:DroneID"`
	Latitude    string `json:"latitude"`
	LatitudeFirstPart    string `json:"latitude_first_part"`
	LatitudeSecondPart    string `json:"latitude_second_part"`
	Longitude    string `json:"longitude"`
	LongitudeFirstPart    string `json:"longitude_first_part"`
	LongitudeSecondPart    string `json:"longitude_second_part"`
	Angle     float64    `json:"angle"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type PermissionArea struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement"`
	DroneID  string
	Drone Drone `gorm:"foreignKey:DroneID"`
	Latitude    string `json:"latitude"`
	Radius     int   `json:"radius"`
/*	LatitudeFirstPart    string `json:"latitude_first_part"`
	LatitudeSecondPart    string `json:"latitude_second_part"`*/
	Longitude    string `json:"longitude"`
/*	LongitudeFirstPart    string `json:"longitude_first_part"`
	LongitudeSecondPart    string `json:"longitude_second_part"`
	Angle     float64    `json:"angle"`*/
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type ViolatorDrones struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement"`
	DroneID  string
	Drone Drone `gorm:"foreignKey:DroneID"`
	PermittedLatitude    string `json:"permitted_latitude"`
	PermittedLongitude    string `json:"permitted_longitude"`
	ActualLatitude    string `json:"actual_latitude"`
	ActualLongitude    string `json:"actual_longitude"`
	PermittedRadius     int   `json:"radius"`
	Displacement  float64 `json:"displacement"`
	ImgPath  string `json:"img_path"`
	/*	LatitudeFirstPart    string `json:"latitude_first_part"`
		LatitudeSecondPart    string `json:"latitude_second_part"`*/

	/*	LongitudeFirstPart    string `json:"longitude_first_part"`
		LongitudeSecondPart    string `json:"longitude_second_part"`
		Angle     float64    `json:"angle"`*/
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}


type City []struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement"`
	Name      string `json:"name"`
	Latitude    string `json:"latitude"`
	Longitude    string `json:"longitude"`
	Country string `json:"city"`
	Iso2             string `json:"iso2"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}


type Countries struct {
	Countries []City `json:""`
}
type CityJson []struct {
	City             string `json:"city"`
	Latitude         string `json:"latitude"`
	Longitude        string `json:"longitude"`
	Country          string `json:"country"`
	Iso2             string `json:"iso2"`
}

func Init()  {

}

func (d Drone) Report(dronePayload forms.DroneReport) (bool, error) {

	db := db.GetDB()



	droneReport := DroneReport{
		Message:   dronePayload.Message,
		Latitude:  dronePayload.Latitude,
		Longitude: dronePayload.Longitude,
		DroneId: uint(dronePayload.DroneId),
		Photo: dronePayload.Photo,
	}
	errDb := db.Create(&droneReport).Error

	if errDb != nil {
		return false, errDb
	}

	return true, nil
}

func  GetPermission(droneId string) (PermissionArea, error) {

	db := db.GetDB()

	var permission PermissionArea
	err := db.First(&permission,droneId).Error// find product with integer primary key

	if err != nil {
		return permission, err
	}

	return permission, nil
}

func  ListReport() ([]DroneReport, error) {

	db := db.GetDB()

	var droneReport []DroneReport
	err := db.Find(&droneReport).Error// find product with integer primary key

	if err != nil {
		return droneReport, err
	}

	return droneReport, nil
}

func (d Drone) SendLiveFlightRecord(flightRecordDataPayload forms.SendDroneFlightLiveData) (bool, error) {



	/*	droneReport := DroneReport{
		Message:   "burda text ola biler",
		Latitude:  "55.5555",
		Longitude: "44.4444",

	}*/
	drone, err := findDroneById(flightRecordDataPayload.DroneId)
	if err!= nil{
		return false, err
	}

	permissionArea, _ := GetPermission(fmt.Sprintf("%d", flightRecordDataPayload.DroneId))


	actualLatitude := flightRecordDataPayload.Latitude
	actualLongitude := flightRecordDataPayload.Longitude
	//userRadius, _ := strconv.Atoi(f.Radius)

	actualCoordinate := Coordinate{
		Latitude:  actualLatitude,
		Longitude: actualLongitude,
	}

	permittedLatitude, _ :=  strconv.ParseFloat(permissionArea.Latitude, 64)
	permittedLongtitude, _ :=  strconv.ParseFloat(permissionArea.Longitude, 64)

	permittedCoordinate := Coordinate{
		Latitude:  permittedLatitude,
		Longitude: permittedLongtitude,
	}

	coordinatesXY := CoordinatesXY{
		Origin:      permittedCoordinate,
		Destination: actualCoordinate,
	}

	distance := Distance(coordinatesXY)

	if int(distance*1000) > permissionArea.Radius {
		var violatorDrone = ViolatorDrones{
			DroneID:            string(flightRecordDataPayload.DroneId),
			PermittedLatitude:  permissionArea.Latitude,
			PermittedLongitude: permissionArea.Longitude,
			ActualLatitude:     fmt.Sprintf("%f", actualLatitude),
			ActualLongitude:    fmt.Sprintf("%f", actualLongitude),
			PermittedRadius:             permissionArea.Radius,
			Displacement: distance*1000,
		}

		_, err :=AddViolatorDrone(violatorDrone)
		if err!= nil{
			return false, err
		}

	}


	lat := fmt.Sprintf("%f", flightRecordDataPayload.Latitude)
	lng := fmt.Sprintf("%f", flightRecordDataPayload.Longitude)
	latitude, err := SplitCoordinate(lat)
	if err!= nil{
		return false, err
	}
	longitude, err := SplitCoordinate(lng)
	if err!= nil{
		return false, err
	}

	flightRecordData := FlightRecord{
		Drone:     drone,
		Latitude:  lat,
		LatitudeFirstPart: latitude.firstPart,
		LatitudeSecondPart: latitude.secondPart,
		Longitude: lng,
		LongitudeFirstPart: longitude.firstPart,
		LongitudeSecondPart: longitude.secondPart,
		Angle: flightRecordDataPayload.Angle,

	}
	db := db.GetDB()
	errDb := db.Create(&flightRecordData).Error

	if errDb != nil {
		return false, errDb
	}

	return true, nil
}

func (d Drone) GetByID(id string) (*Drone, error) {

	db := db.GetDB()
	drone := Drone{}
	errDb := db.First(&drone, id).Error
	if errDb != nil {
		return nil, errors.New("Drone Id  doesnt exist")
	}
	return &drone, nil
}

func (d Drone) GetAll() (*[]Drone, error) {

	db := db.GetDB()
	drones := []Drone{}
	errDb := db.Find(&drones).Error
	if errDb != nil {
		return nil, errors.New("Drone Id  doesnt exist")
	}
	return &drones, nil
}

func findDroneById(droneId uint) (Drone, error) {
	db := db.GetDB()
	drone := Drone{}
	errDb := db.First(&drone, droneId).Error
	if errDb != nil {
		return drone, errors.New("Drone Id  doesnt exist")
	}
	return drone, nil
}

func (d Drone) AddDevice(drone forms.Drone) (bool, error) {

	db := db.GetDB()

	/*	droneReport := DroneReport{
		Message:   "burda text ola biler",
		Latitude:  "55.5555",
		Longitude: "44.4444",

	}*/

	drone.DroneId = uuid.New()
	errDb := db.Create(&drone).Error

	if errDb != nil {
		return false, errDb
	}

	return true, nil
}


func  AddViolatorDrone(violatorDrone ViolatorDrones) (bool, error) {

	db := db.GetDB()

	/*	droneReport := DroneReport{
		Message:   "burda text ola biler",
		Latitude:  "55.5555",
		Longitude: "44.4444",

	}*/

	//drone.DroneId = uuid.New()
	errDb := db.Create(&violatorDrone).Error

	if errDb != nil {
		return false, errDb
	}

	return true, nil
}