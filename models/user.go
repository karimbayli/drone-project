package models

import (
	"drone-project/db"
	"drone-project/forms"
	"errors"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model `json:"-"`
	ID        uint `gorm:"primaryKey;autoIncrement;not null"`
	UsrCode   uuid.UUID  `json:"code", gorm:"primaryKey;autoIncrement;not null"`
	Name      string `json:"name"`
	BirthDay  string `json:"birthday"`
	Email  string     `gorm:"unique"`
	RoleID int  `json:"role_id"`
	Role Role  `json:"-" gorm:"references:ID"`
	Password  string `json:"-"`
	Gender    string `json:"gender"`
	PhotoURL  string `json:"-"`
	Time      int64  `json:"-"`
	Active    bool   `json:"-,omitempty"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

type Role struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement;not null"`
	Name      string `json:"name"; gorm:"unique"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

/*type UserGroup struct {
	gorm.Model
	ID        uint `gorm:"primaryKey;autoIncrement;not null"`
	Role  string // admin, user, moderator, super user

	UpdatedAt int64  `json:"updated_at,omitempty", gorm:"autoUpdateTime:milli`
	Created   int64 `gorm:"autoCreateTime"`
}*/


func (h User) Signup(userPayload forms.UserSignup) (*User, error) {
	db := db.GetDB()
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(userPayload.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	user := User{
		Name:      userPayload.Name,
		Password: string(passwordHash),
		BirthDay:  userPayload.BirthDay,
		Gender:    userPayload.Gender,
		UsrCode: uuid.New(),
		Email:    userPayload.Email,
		RoleID: 1,
		Time:      time.Now().UnixNano(),
		Active:    true,
	}
	errDb := db.Create(&user).Error
/*	fmt.Println(reflect.TypeOf(errDb.Error()))
	errorString := "UNIQUE constraint failed: users.email"
	if errDb.Error() == errorString {
		return nil, err
	}*/
	if errDb != nil {
		return nil, errDb
	}
	return &user, nil
}

func (h User) GetByID(id string) (*User, error) {
	var user User
	db := db.GetDB()
	err := db.First(&user,id).Error// find product with integer primary key
	errors.Is(err, gorm.ErrRecordNotFound)

	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (h User) Login(loginPayload forms.LoginCredentials) (*User, error) {
	var user User
/*	if !h.EmailExists(loginPayload.Email){
		return nil, errors.New("not registered")
	}*/
	db := db.GetDB()

	err := db.First(&user,"email", loginPayload.Email).Error// find product with integer primary key

	if err != nil {
		return nil, errors.New("wrong mail")
	}
	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginPayload.Password))
	if errf != nil {
		return nil, errors.New("wrong password")
	}
	return &user, nil
}

func (h User) EmailExists(email string) bool {
	var user User
	db := db.GetDB()
	err := db.First(&user,"email",email).Error// find product with integer primary key
	if err != nil {
		return false
	}
	return true
}


