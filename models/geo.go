package models

import (
	"errors"
	"math"
	"regexp"
	"strings"
)

type CoordinatesXY struct {
	Origin Coordinate
	Destination Coordinate
}


type Coordinate struct {
	Latitude  float64
	Longitude float64
}
const radius = 6371 // Earth's mean radius in kilometers

type GeoService interface {
	Distance(coordinates CoordinatesXY) float64
}

func degrees2radians(degrees float64) float64 {
	return degrees * math.Pi / 180
}

func Distance(coordinates CoordinatesXY) float64 {
	degreesLat := degrees2radians(coordinates.Destination.Latitude - coordinates.Origin.Latitude)
	degreesLong := degrees2radians(coordinates.Destination.Longitude - coordinates.Origin.Longitude)
	a := math.Sin(degreesLat/2)*math.Sin(degreesLat/2) +
		math.Cos(degrees2radians(coordinates.Origin.Latitude))*
			math.Cos(degrees2radians(coordinates.Destination.Latitude))*math.Sin(degreesLong/2)*
			math.Sin(degreesLong/2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	d := radius * c

	return d
}

type CoordinateDot struct {
	firstPart string
	secondPart string
}

func SplitCoordinate(coordinate string) ( CoordinateDot, error){
	if coordinate=="" || !strings.Contains(coordinate,".") {
		return CoordinateDot{}, errors.New("wrong coordinate format")
	}
	re := regexp.MustCompile(`[.]`)

	// Split based on pattern.
	// ... Second argument means "no limit."
	result := re.Split(coordinate, -1)
	coordinateDot := CoordinateDot{
		firstPart:  result[0],
		secondPart: result[1],
	}
	return coordinateDot, nil


}
