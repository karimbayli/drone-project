package server

import (
	"drone-project/forms"
	"drone-project/models"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)
var droneModel = new(models.Drone)

type connection struct {
	// Buffered channel of outbound messages.
	send chan []byte

	// The hub.
	h *hub
}

func (c *connection) reader(wg *sync.WaitGroup, wsConn *websocket.Conn) {
	defer wg.Done()
	for {
		var sendDroneFlightLiveData forms.SendDroneFlightLiveData
		err := wsConn.ReadJSON(&sendDroneFlightLiveData)
		if err != nil {
			break
		}
		var jsonData []byte
		jsonData, err1 := json.Marshal(sendDroneFlightLiveData)
		if err1 != nil {
			log.Println(err)
		}
		c.h.broadcast <- jsonData
		go droneModel.SendLiveFlightRecord(sendDroneFlightLiveData)

	}
}

type flight struct {
	DroneId uint `json:"drone_id"`
	Latitude  float64 `json:"latitude"`
	Longitude  float64 `json:"longitude"`
	Angle  int `json:"angle"`
}

func (c *connection) writer(wg *sync.WaitGroup, wsConn *websocket.Conn) {
	defer wg.Done()
	for message := range c.send {

		bytes := []byte(message)

		var p flight
		err := json.Unmarshal(bytes, &p)
		if err != nil {
			fmt.Println(err)
		}
		err1 := wsConn.WriteJSON(p)
		if err1 != nil {
			fmt.Println(err)
		}

		//err := wsConn.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			break
		}
	}
}

var upgrader = &websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024,CheckOrigin: func(r *http.Request) bool {
	return true
}}

type wsHandler struct {
	h *hub
}

func (wsh wsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	wsConn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("error upgrading %s", err)
		return
	}
	c := &connection{send: make(chan []byte, 256), h: wsh.h}
	c.h.addConnection(c)
	defer c.h.removeConnection(c)
	var wg sync.WaitGroup
	wg.Add(2)
	go c.writer(&wg, wsConn)
	go c.reader(&wg, wsConn)
	wg.Wait()
	wsConn.Close()
}
