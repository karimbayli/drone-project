package server

import (
	"drone-project/controllers"
	"drone-project/middlewares"
	"drone-project/services"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	_ "github.com/swaggo/swag/example/celler/controller"
	_ "github.com/swaggo/swag/example/celler/docs"
	_ "github.com/swaggo/swag/example/celler/httputil"
	"net/http"

	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewRouter() *gin.Engine{
	var loginService = services.StaticLoginService()
	var jwtService = services.JWTAuthService()
	var loginController = controllers.LoginHandler(loginService, jwtService)
	router := gin.New()

	//config := cors.DefaultConfig()
	//config.AllowOrigins = []string{"*"}

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:     []string{"POST", "GET","OPTIONS","PUT","DELETE"},
		AllowHeaders:    []string{"Access-Control-Allow-Headers", "Accept", "Content-Type","Content-Length","Accept-Encoding","X-CSRF-Token","Authorization"},
		AllowWebSockets: true,
	}))

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	health := new(controllers.HealthController)
	router.GET("/health", health.Status)

	//-------------------------------------------------
	ws := new(controllers.WsController)
	router.GET("/json", ws.JsonApi)
	router.GET("/text", ws.TextApi)

	// static files
	router.Use(static.Serve("/", static.LocalFile("./public", true)))

	router.NoRoute(func(c *gin.Context) {
		//c.Redirect(http.StatusMovedPermanently, "https://oxu.az/showbiz/423319")
		c.File("./public/index.html")
	})

	//-------------------------------------------------
	//router.Use(middlewares.AuthMiddleware())

	v1 := router.Group("v1")
	{
		userGroup := v1.Group("user")
		{
			user := new(controllers.UserController)

			userGroup.GET("/:id", user.Retrieve)
			userGroup.POST("/register", user.Register)
			userGroup.POST("/login", func(ctx *gin.Context) {
				token := loginController.Login(ctx)
				if token != "" {
					ctx.JSON(http.StatusOK, gin.H{
						"token": token,
					})
				} else {
					ctx.JSON(http.StatusUnauthorized, nil)
				}
			})



		}
		fileGroup := v1.Group("file")
		{
			upload := new(controllers.UploadController)
			fileGroup.POST("/upload",upload.Upload )
			fileGroup.GET("/:file", upload.GetFile)


		}

		processGroup := v1.Group("process")
		{
			upload := new(controllers.UploadController)
			processGroup.GET("/:file", upload.Process)
		}

		reportGroup := v1.Group("report")
		{
			drone := new(controllers.DroneController)
			reportGroup.GET("/list", drone.ListReport)
		}

		machineGroup := v1.Group("machine")
		{
			drone := new(controllers.DroneController)
			machineGroup.GET("/list",drone.List)
		}


		droneGroup := v1.Group("drone")
		droneGroup.Use(middlewares.AuthorizeJWT())
		{
			drone := new(controllers.DroneController)
			droneGroup.POST("/report", drone.Report)
			droneGroup.POST("/add", drone.AddDevice)
			droneGroup.GET("/:id", drone.Retrieve)
/*			droneGroup.GET("/list", func(c *gin.Context) {
				drone.ServeHTTP(c.Writer, c.Request)
			})*/
/*			droneGroup.POST("/report", func(ctx *gin.Context) {
				token := drone.Report(ctx)
				if token != "" {
					ctx.JSON(http.StatusOK, gin.H{
						"token": token,
					})
				} else {
					ctx.JSON(http.StatusUnauthorized, nil)
				}
			})*/
		}
		h :=newHub()
		dataLiveGroup := v1.Group("data-live")
		{
			liveData := new(controllers.LiveDataController)
			dataLiveGroup.GET("/sendLive",liveData.SendLive)
			dataLiveGroup.GET("/firelog",liveData.FlightStats)
			dataLiveGroup.GET("/ws", func(c *gin.Context) {
				handler := wsHandler{h: h}
				handler.ServeHTTP(c.Writer, c.Request)
			})
		}
	}



	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return router
}
