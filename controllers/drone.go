package controllers

import (
	"bytes"
	"drone-project/forms"
	"drone-project/models"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/swaggo/swag/example/celler/httputil"
	"io/ioutil"
	"log"
	"net/http"
)

type DroneController struct {


}

var droneModel = new(models.Drone)


func (l DroneController) ListReport(c *gin.Context) {



	reports, err := models.ListReport()

	if err!=nil{
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, reports)



	/*	if c.Param("id") != "" {
			user, err := userModel.GetByID(c.Param("id"))
			if err != nil {
				httputil.NewError(c, http.StatusBadRequest, err)
				return
			}
			c.JSON(http.StatusOK, user)
		}
		httputil.NewError(c, http.StatusBadRequest, nil)
		return*/

}

func (d DroneController) Retrieve(c *gin.Context) {
	if c.Param("id") != "" {
		drone, err := droneModel.GetByID(c.Param("id"))
		if err != nil {
			httputil.NewError(c, http.StatusBadRequest, err)
			return
		}
		c.JSON(http.StatusOK, drone)
	}
	httputil.NewError(c, http.StatusBadRequest, nil)
	return
}

func (d DroneController) List(c *gin.Context) {
		drone, err := droneModel.GetAll()
		if err != nil {
			httputil.NewError(c, http.StatusBadRequest, err)
			return
		}
		c.JSON(http.StatusOK, drone)
}


func (d DroneController) ServeHTTP(w http.ResponseWriter, r *http.Request){
	drone, err := droneModel.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(drone)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

/*func (d DroneController) List(c *gin.Context) {
	if c.Param("id") != "" {
		user, err := droneModel.GetByID(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user", "error": err})
			c.Abort()
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "User founded!", "user": user})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "bad request"})
	c.Abort()
	return
}

func (d DroneController) Report(c *gin.Context) {
	if c.Param("id") != "" {
		user, err := droneModel.GetByID(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user", "error": err})
			c.Abort()
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "User founded!", "user": user})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "bad request"})
	c.Abort()
	return
}


func (d DroneController) ListReport(c *gin.Context) {
	if c.Param("id") != "" {
		user, err := droneModel.GetByID(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user", "error": err})
			c.Abort()
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "User founded!", "user": user})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "bad request"})
	c.Abort()
	return
}*/

func (d DroneController) Report(c *gin.Context) {

 // Write body back
	//fmt.Println(reqBody)
	var droneReport forms.DroneReport
	var bodyBytes []byte
	if c.Request.Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
	}
	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	log.Println("second", string(bodyBytes))
	bindError := c.ShouldBindBodyWith(&droneReport,binding.JSON)
	if bindError != nil {
		log.Println("err: ",bindError)
		c.JSON(406, gin.H{"message": "Invalid signin form", "form": forms.DroneReport{}})
		return
	}
	drone, err := droneModel.Report(droneReport)
	if err != nil {
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusCreated, drone)

}

func (d DroneController) AddDevice(c *gin.Context) {

	// Write body back
	//fmt.Println(reqBody)
	var drone forms.Drone
	var bodyBytes []byte
	if c.Request.Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
	}
	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	log.Println("second", string(bodyBytes))
	bindError := c.ShouldBindBodyWith(&drone,binding.JSON)
	if bindError != nil {
		log.Println("err: ",bindError)
		c.JSON(406, gin.H{"message": "Invalid signin form", "form": forms.Drone{}})
		return
	}
	droneR, err := droneModel.AddDevice(drone)
	if err != nil {
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusCreated, droneR)

}

func (d DroneController) Live(c *gin.Context) {

	// Write body back
	//fmt.Println(reqBody)
	var droneFlightLiveData forms.SendDroneFlightLiveData
	var bodyBytes []byte
	if c.Request.Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
	}
	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))


	bindError := c.ShouldBindBodyWith(&droneFlightLiveData,binding.JSON)
	if bindError != nil {
		log.Println("err: ",bindError)
		c.JSON(406, gin.H{"message": "Invalid signin form", "form": forms.SendDroneFlightLiveData{}})
		return
	}
	drone, err := droneModel.SendLiveFlightRecord(droneFlightLiveData)
	if err != nil {
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusCreated, drone)

}
