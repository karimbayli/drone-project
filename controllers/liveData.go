package controllers

import (
	"drone-project/forms"
	"drone-project/models"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/swag/example/celler/httputil"
	"log"
	"net/http"
	"strconv"
	"time"
)

type LiveDataController struct{}


var flightStats = new(models.FlightStats)

func (l LiveDataController) SendLive(c *gin.Context) {

	// Write body back
	//fmt.Println(reqBody)
	latitude := c.Query("lat")
	longitude := c.Query("lon")
	droneId := c.Query("drone_id")

	latitudeF, _ := strconv.ParseFloat(latitude, 8)
	longitudeF, _ := strconv.ParseFloat(longitude, 8)
	droneIdF, _ := strconv.ParseUint(droneId, 10, 8)



	var sendDroneFlightLiveData = forms.SendDroneFlightLiveData{
		DroneId:   uint(droneIdF),
		Latitude:  latitudeF,
		Longitude: longitudeF,
		Angle:     0,
	}

	_, err := droneModel.SendLiveFlightRecord(sendDroneFlightLiveData)
	if err != nil {
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}
	c.String(200,"created")

}




func (l LiveDataController) FlightStats(c *gin.Context) {

	latitude := c.DefaultQuery("lat","40.3942544")
	longitude := c.DefaultQuery("lon","49.5747749")
	radius := c.DefaultQuery("r","5000")

	flightStats.Latitude = latitude
	flightStats.Longitude = longitude
	flightStats.Radius = radius

	drones, err := flightStats.FindNearestDrones()

	if err!=nil{
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, drones)



	/*	if c.Param("id") != "" {
			user, err := userModel.GetByID(c.Param("id"))
			if err != nil {
				httputil.NewError(c, http.StatusBadRequest, err)
				return
			}
			c.JSON(http.StatusOK, user)
		}
		httputil.NewError(c, http.StatusBadRequest, nil)
		return*/

}


func ( lws LiveDataController)FlightStatsWs(c *gin.Context) {

	latitude := c.DefaultQuery("lat","40.3942544")
	longitude := c.DefaultQuery("lon","49.5747749")
	radius := c.DefaultQuery("r","5000")

	flightStats.Latitude = latitude
	flightStats.Longitude = longitude
	flightStats.Radius = radius


	//Upgrade get request to webSocket protocol
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		httputil.NewError(c, http.StatusBadRequest, err)
		return
	}
	defer ws.Close()

	for {
/*		if err := ws.Close(); err != nil {
			return
		}*/
		drones, err := flightStats.FindNearestDrones()
		if err!=nil{
			httputil.NewError(c, http.StatusBadRequest, err)
			return
		}

		if drones ==nil{
			err = ws.WriteJSON(make([]string, 0))
			if err != nil {
				log.Println("error write json: " + err.Error())
			}
		}else {
			err = ws.WriteJSON(drones)
			if err != nil {
				log.Println("error write json: " + err.Error())
			}
		}

		time.Sleep(1 * time.Second)
	}
}