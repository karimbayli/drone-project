package controllers

import (
	"drone-project/forms"
	"drone-project/models"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/swaggo/swag/example/celler/httputil"
	"log"
	"net/http"
)

type UserController struct{}

var userModel = new(models.User)



// ShowUser godoc
// @Summary Show an user
// @Description get string by ID
// @Tags user
// @Accept  json
// @Produce  json
// @Param id path int true "User ID"
// @Success 200 {object} models.User
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /user/{id} [get]
func (u UserController) Retrieve(c *gin.Context) {
	if c.Param("id") != "" {
		user, err := userModel.GetByID(c.Param("id"))
		if err != nil {
			httputil.NewError(c, http.StatusBadRequest, err)
			return
		}
		c.JSON(http.StatusOK, user)
	}
	httputil.NewError(c, http.StatusBadRequest, nil)
	return
}


func (u UserController) Register(c *gin.Context) {
	//jsonData, err := ioutil.ReadAll(c.Request.Body)
	fmt.Println(c.Request.Body)
	var userSignUp forms.UserSignup
	bindError := c.ShouldBindBodyWith(&userSignUp,binding.JSON)
	if bindError != nil {
		log.Println("err: ",bindError)
		c.JSON(406, gin.H{"message": "Invalid signin form", "form": forms.UserSignup{}})
		return
	}
	user, err := userModel.Signup(userSignUp)
	if err != nil {
		httputil.NewError(c, http.StatusBadRequest, errors.New("User with this email already registered"))
		return
	}
	userRegisterResponse := forms.UserRegisterResponse{
		Code: user.UsrCode.String(),
		Message: "Successful user registration.",
	}
	c.JSON(http.StatusOK, userRegisterResponse)

}



