package controllers

import (
	"bytes"
	"drone-project/models"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/swag/example/celler/httputil"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"
)
type UploadController struct {


}


var uploadModel = new(models.Upload)


func (u UploadController) Upload(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	log.Println(file.Filename)
	fileName := time.Now().Format("20060102150405")+file.Filename
	err = c.SaveUploadedFile(file, "uploads/"+fileName )
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	var fileNameRes = models.UploadFileResponse{FileName: fileName}
	c.JSON(http.StatusOK, fileNameRes)
}


func (u UploadController) GetFile(c *gin.Context) {

	if c.Param("file") != "" {
		fileName:= c.Param("file")
		path := "uploads/" + fileName
		if _, err := os.Stat(path); os.IsNotExist(err) {
			c.Status(404)
			return
		}


		log.Println(fileName)

		c.File(path)
	}
}

func (u UploadController) Process(c *gin.Context) {

	if c.Param("file") != "" {
		fileName:= c.Param("file")
		path := "uploads/" + fileName
		if _, err := os.Stat(path); os.IsNotExist(err) {
			c.Status(404)
			return
		}

		processFile, err := sendPostRequest("http://34.72.91.253:5000/uploader",path,"")
		if err != nil {
			log.Println(err)
			httputil.NewError(c, http.StatusBadRequest, err)
			return
		}

		//permissions := 0644 // or whatever you need
		byteArray := processFile
		processedFile := time.Now().Format("20060102150405")+".png"
		err1 := ioutil.WriteFile("uploads/" + processedFile, byteArray, os.FileMode(0644))
		if err1 != nil {
			httputil.NewError(c, http.StatusBadRequest, err)
		}

		var fileNameRes = models.UploadFileResponse{FileName: processedFile}
		c.JSON(http.StatusOK, fileNameRes)
	}
}



func sendPostRequest (url string, filename string, filetype string) ([]byte, error) {
	file, err := os.Open(filename)

	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer file.Close()


	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))

	if err != nil {
		log.Println(err)
		return nil, err
	}

	io.Copy(part, file)
	writer.Close()
	request, err := http.NewRequest("POST", url, body)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	request.Header.Add("Content-Type", writer.FormDataContentType())
	client := &http.Client{}

	response, err := client.Do(request)

	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer response.Body.Close()

	content, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Println(err)
		return nil, err
	}
	text := string(content)
	if text=="{\n  \"status\": \"no drone is found\"\n}\n"{
		return nil, errors.New("no drone is found")
	}
	return content, nil
}