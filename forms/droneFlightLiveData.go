package forms

type SendDroneFlightLiveData struct {
	DroneId uint  `json:"drone_id"  binding:"required"`
	//radius string `json:"radius"  binding:"required"`
	Latitude    float64 `json:"latitude"  binding:"required"`
	Longitude    float64 `json:"longitude"  binding:"required"`
	Angle      float64    `json:"angle"  `
}

type GetDroneFlightLiveData struct {
	radius string `json:"radius"  binding:"required"`
	Latitude    string `json:"latitude"  binding:"required"`
	Longitude    string `json:"longitude"  binding:"required"`
}