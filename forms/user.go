package forms

import "github.com/google/uuid"

type UserSignup struct {
	Name     string `json:"name" binding:"required"`
	UsrCode   uuid.UUID  `json:"code"`
	BirthDay string `json:"birthday" binding:"required"`
	Gender   string `json:"gender" binding:"required"`
	Password  string `binding:"required`
	Email  string `json:"email"  binding:"required"`
}

type UserRegisterResponse struct {
	Code     string `json:"code" `
	Message string `json:"message" `
	NotificationChannel   string `json:"notificationChannel" `
}


