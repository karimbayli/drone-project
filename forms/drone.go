package forms

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type DroneReport struct {
	Message string `json:"message"  binding:"required"`
	Latitude    float64 `json:"latitude"  binding:"required"`
	Longitude    float64 `json:"longitude"  binding:"required"`
	Photo       string `json:"img_path"`
	DroneId    int `json:"drone_id"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

type DroneLive struct {
	Message string `json:"message"  binding:"required"`
	Latitude    string `json:"latitude"  binding:"required"`
	Longitude    string `json:"longitude"  binding:"required"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

type Drone struct {
	DroneId   uuid.UUID  `json:"-", binding:"required"`
	Name      string `json:"name" binding:"required"`
	Issuer      string   `json:"issuer" binding:"required"`
	SerialNumber      string  `json:"serial_number" binding:"required"`
	DroneTypeID    int `json:"type_id" binding:"required"`
	//User    User    `gorm:"foreignKey:ID"`
	//OwnerId    string `json:"owner_id"`
	OwnerName    string `json:"owner_name" binding:"required"`
	Serial      string  `json:"serial" binding:"required"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt gorm.DeletedAt ` json:"-";gorm:"index"`
}