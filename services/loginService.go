package services

import (
	"drone-project/forms"
	"drone-project/models"
)

type LoginService interface {
	LoginUser(credential forms.LoginCredentials) (bool,  *models.User)
}
var userModel = new(models.User)

type loginInformation struct {
	email    string
	password string
}

func StaticLoginService() LoginService {
	return &loginInformation{}
}
func (info *loginInformation) LoginUser(credential forms.LoginCredentials) (bool, *models.User) {
	user , err :=userModel.Login(credential)
	if err != nil{
		return false, nil
	}
	return true, user
}